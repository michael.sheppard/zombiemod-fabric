/*
 * AbstractPigZombieEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.zombiemod.common;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.class_5493;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.brain.MemoryModuleType;
import net.minecraft.entity.ai.pathing.MobNavigation;
import net.minecraft.entity.ai.pathing.PathNodeType;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.PiglinActivity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.network.DebugInfoSender;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

public abstract class AbstractPigZombieEntity extends HostileEntity {
    protected static final TrackedData<Boolean> IMMUNE_TO_ZOMBIFICATION;
//    protected int timeInOverworld = 0;

    public AbstractPigZombieEntity(EntityType<? extends AbstractPigZombieEntity> entityType, World world) {
        super(entityType, world);
        this.setCanPickUpLoot(true);
        this.setCanPathThroughDoors();
        this.setPathfindingPenalty(PathNodeType.DANGER_FIRE, 16.0F);
        this.setPathfindingPenalty(PathNodeType.DAMAGE_FIRE, -1.0F);
    }

    private void setCanPathThroughDoors() {
        if (class_5493.method_30955(this)) {
            ((MobNavigation) this.getNavigation()).setCanPathThroughDoors(true);
        }
    }

//    protected abstract boolean canHunt();

    public void setImmuneToZombification(boolean immuneToZombification) {
        this.getDataTracker().set(IMMUNE_TO_ZOMBIFICATION, immuneToZombification);
    }

    protected boolean isImmuneToZombification() {
        return this.getDataTracker().get(IMMUNE_TO_ZOMBIFICATION);
    }

    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(IMMUNE_TO_ZOMBIFICATION, false);
    }

    public void writeCustomDataToTag(CompoundTag tag) {
        super.writeCustomDataToTag(tag);
        if (this.isImmuneToZombification()) {
            tag.putBoolean("IsImmuneToZombification", true);
        }

//        tag.putInt("TimeInOverworld", this.timeInOverworld);
    }

    public double getHeightOffset() {
        return this.isBaby() ? -0.05D : -0.45D;
    }

    public void readCustomDataFromTag(CompoundTag tag) {
        super.readCustomDataFromTag(tag);
        this.setImmuneToZombification(tag.getBoolean("IsImmuneToZombification"));
//        this.timeInOverworld = tag.getInt("TimeInOverworld");
    }

//    protected void mobTick() {
//        super.mobTick();
//        if (this.shouldZombify()) {
//            ++this.timeInOverworld;
//        } else {
//            this.timeInOverworld = 0;
//        }
//
//        if (this.timeInOverworld > 300) {
//            this.playZombificationSound();
//            this.zombify((ServerWorld)this.world);
//        }
//
//    }

//    public boolean shouldZombify() {
//        return true;
//    }

//    protected void zombify(ServerWorld world) {
//        ZombifiedPiglinEntity zombifiedPiglinEntity = (ZombifiedPiglinEntity)this.method_29243(EntityType.ZOMBIFIED_PIGLIN, true);
//        if (zombifiedPiglinEntity != null) {
//            zombifiedPiglinEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.NAUSEA, 200, 0));
//        }
//    }

//    public boolean isAdult() {
//        return !this.isBaby();
//    }

    @Environment(EnvType.CLIENT)
    public abstract PiglinActivity getActivity();

    @Nullable
    public LivingEntity getTarget() {
        return this.brain.getOptionalMemory(MemoryModuleType.ATTACK_TARGET).orElse(null);
    }

//    protected boolean isHoldingTool() {
//        return this.getMainHandStack().getItem() instanceof ToolItem;
//    }

//    public void playAmbientSound() {
//        if (PiglinBrain.hasIdleActivity(this)) {
//            super.playAmbientSound();
//        }
//
//    }

    protected void sendAiDebugData() {
        super.sendAiDebugData();
        DebugInfoSender.sendBrainDebugData(this);
    }

//    protected abstract void playZombificationSound();

    static {
        IMMUNE_TO_ZOMBIFICATION = DataTracker.registerData(AbstractPigZombieEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
    }
}
