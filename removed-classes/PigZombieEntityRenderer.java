/*
 * PigZombieEntityRenderer.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.zombiemod.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.BipedEntityRenderer;
import net.minecraft.client.render.entity.EntityRenderDispatcher;
import net.minecraft.client.render.entity.feature.ArmorFeatureRenderer;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class PigZombieEntityRenderer extends BipedEntityRenderer<MobEntity, PigZombieEntityModel<MobEntity>> {
    private static final Identifier PIGZOMBIE_SKIN = new Identifier("textures/entity/piglin/zombified_piglin.png");

    public PigZombieEntityRenderer(EntityRenderDispatcher dispatcher, boolean zombified) {
        super(dispatcher, getPiglinModel(zombified), 0.5F, 1.0019531F, 1.0F, 1.0019531F);
        this.addFeature(new ArmorFeatureRenderer<>(this, new BipedEntityModel<>(0.5F), new BipedEntityModel<>(1.02F)));
    }

    private static PigZombieEntityModel<MobEntity> getPiglinModel(boolean zombified) {
        PigZombieEntityModel<MobEntity> pigZombieEntityModel = new PigZombieEntityModel<>(0.0F, 64, 64);
        if (zombified) {
            pigZombieEntityModel.leftEar.visible = false;
        }

        return pigZombieEntityModel;
    }

    public Identifier getTexture(MobEntity mobEntity) {
        return PIGZOMBIE_SKIN;
    }


}
