/*
 * AbstractZombieModEntityModel.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.zombiemod.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.model.BipedEntityModel;
import net.minecraft.client.render.entity.model.CrossbowPosing;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.util.math.MathHelper;

import static net.minecraft.client.render.entity.model.CrossbowPosing.method_29350;

@Environment(EnvType.CLIENT)
public abstract class AbstractZombieModEntityModel <T extends PathAwareEntity> extends BipedEntityModel<T> {
    protected AbstractZombieModEntityModel(float f, float g, int i, int j) {
        super(f, g, i, j);
    }

    public void setAngles(T entity, float f, float g, float h, float i, float j) {
        super.setAngles(entity, f, g, h, i, j);

        float sinAngle = MathHelper.sin(f * 3.1415927F);
        float sinClamp = MathHelper.sin((1.0F - (1.0F - f) * (1.0F - f)) * 3.1415927F);
        rightArm.roll = 0.0F;
        leftArm.roll = 0.0F;
        rightArm.yaw = -(0.1F - sinAngle * 0.6F);
        leftArm.yaw = 0.1F - sinAngle * 0.6F;
        float armAngle = isAttacking(entity) ? -2.0943951333f : 0.0f;
        rightArm.pitch = armAngle;
        leftArm.pitch = armAngle;
        rightArm.pitch += sinAngle * 1.2F - sinClamp * 0.4F;
        leftArm.pitch += sinAngle * 1.2F - sinClamp * 0.4F;
        method_29350(rightArm, leftArm, g);
        CrossbowPosing.method_29352(leftArm, rightArm, isAttacking(entity), handSwingProgress, sinAngle);
    }

    public abstract boolean isAttacking(T entity);
}
