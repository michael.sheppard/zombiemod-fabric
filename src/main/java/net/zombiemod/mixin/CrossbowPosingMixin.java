/*
 * CrossbowPosingMixin.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.zombiemod.mixin;

//import net.minecraft.client.model.ModelPart;
//import net.minecraft.client.render.entity.model.CrossbowPosing;
//import net.minecraft.util.math.MathHelper;
//import org.spongepowered.asm.mixin.Mixin;
//import org.spongepowered.asm.mixin.Overwrite;
//
//import static net.minecraft.client.render.entity.model.CrossbowPosing.method_29350;
//
//@Mixin(CrossbowPosing.class)
//public abstract class CrossbowPosingMixin {
//
//    /**
//     * @author crackedEgg
//     *
//     * @reason  the zombies should hang their arms down unless they are attacking
//     */
//    @Overwrite
//    public static void method_29352(ModelPart modelPart, ModelPart modelPart2, boolean attacking, float f, float g) {
//        float h = MathHelper.sin(f * 3.1415927F);
//        float i = MathHelper.sin((1.0F - (1.0F - f) * (1.0F - f)) * 3.1415927F);
//        modelPart2.roll = 0.0F;
//        modelPart.roll = 0.0F;
//        modelPart2.yaw = -(0.1F - h * 0.6F);
//        modelPart.yaw = 0.1F - h * 0.6F;
//        float j = attacking ? -2.0943951333f : 0.0f;
//        modelPart2.pitch = j;
//        modelPart.pitch = j;
//        modelPart2.pitch += h * 1.2F - i * 0.4F;
//        modelPart.pitch += h * 1.2F - i * 0.4F;
//        method_29350(modelPart2, modelPart, g);
//    }
//}
