/*
 * ZombieModEntity.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package net.zombiemod.common;

import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityPose;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributeInstance;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.mob.HostileEntity;
import net.minecraft.entity.mob.ZombieEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

public class ZombieModEntity extends ZombieEntity {
    public ZombieModEntity(EntityType<? extends ZombieModEntity> entityType, World world) {
        super(entityType, world);
    }

    public static DefaultAttributeContainer.Builder createAttributes() {
        return HostileEntity.createHostileAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 16.0D)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.23D)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 3.0D);
    }

    // implementing Biome specific spawning here
    @SuppressWarnings("unused")
    public static boolean canSpawn(EntityType<ZombieModEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random) {
        return validSpawnBiomes(world, pos);
    }

    private static boolean validSpawnBiomes(WorldAccess world, BlockPos pos) {
        Optional<RegistryKey<Biome>> optional = world.method_31081(pos);
        return (Objects.equals(optional, Optional.of(BiomeKeys.PLAINS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.BIRCH_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.FLOWER_FOREST)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.MOUNTAINS)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.GIANT_SPRUCE_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.SNOWY_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.GIANT_TREE_TAIGA)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.DESERT)) ||
                Objects.equals(optional, Optional.of(BiomeKeys.DESERT_HILLS))) &&
                (world.getBlockState(pos.down()).isOf(Blocks.GRASS) ||
                world.getBlockState(pos.down()).isOf(Blocks.STONE) ||
                world.getBlockState(pos.down()).isOf(Blocks.SAND));
    }

    @Override
    protected boolean burnsInDaylight() {
        return false;
    }

    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return this.isBaby() ? 0.93F : 1.74F;
    }

    @Override
    protected void initAttributes() {
        EntityAttributeInstance attributeInstance = getAttributeInstance(EntityAttributes.ZOMBIE_SPAWN_REINFORCEMENTS);
        if (attributeInstance != null) {
            attributeInstance.setBaseValue(this.random.nextDouble() * 0.1);
        }
    }
}
