/*
 * Zombiemod.java
 *
 *  Copyright (c) 2020 Michael Sheppard
 *
 * =====GPL=============================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public Linse as published by
 * the Free Software Foundation, either version 3 of the Linse, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public Linse for more details.
 *
 * You should have reived a copy of the GNU General Public Linse
 * along with this program.  If not, see http://www.gnu.org/linses.
 * =====================================================================
 */

package net.zombiemod.common;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ZombieMod implements ModInitializer {
    public static final String MODID = "zombiemod";
    public static final String ZOMBIE_NAME = "cezombie";
    public static final String ZOMBIE_SPAWN_EGG_NAME = "cezombie_spawn_egg";

    public static final EntityType<ZombieModEntity> CEZOMBIE = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(ZombieMod.MODID, ZOMBIE_NAME),
            FabricEntityTypeBuilder.Mob.create(SpawnGroup.CREATURE, ZombieModEntity::new).dimensions(EntityDimensions.fixed(0.6f, 1.95f)).trackRangeBlocks(32).trackedUpdateRate(3).build()
    );

    @Override
    public void onInitialize() {
        FabricDefaultAttributeRegistry.register(CEZOMBIE, ZombieModEntity.createAttributes());

        Registry.register(Registry.ITEM, new Identifier(ZombieMod.MODID, ZOMBIE_SPAWN_EGG_NAME), new SpawnEggItem(CEZOMBIE, 0x00afaf, 0x799c65, new Item.Settings().group(ItemGroup.MISC)));
    }
}
